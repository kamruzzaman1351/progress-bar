// UI Variables
const progrssUI = document.getElementById("progress");
const previousBtnUI = document.getElementById("prev");
const nextBtnUI = document.getElementById("next");
const circleUI = document.querySelectorAll(".circle");

let currentActive = 1;
// Next Btn Listener Events
nextBtnUI.addEventListener("click", () => {
   currentActive++;
    if(currentActive > circleUI.length) {
        currentActive = circleUI.length;        
    }
    updated();
    
});
// Previous Btn Listener Events
previousBtnUI.addEventListener("click", () => {
   currentActive--;
    if(currentActive < 1) {
        currentActive = 1;
        
    } 
    updated();
    // console.log(currentActive);
    
});

// Update the progress bar
function updated() {
    circleUI.forEach((circle, index) => {
        if(index < currentActive) {
            circle.classList.add("active");
        } else {
            circle.classList.remove("active");
        }        
    })
    const activeUI = document.querySelectorAll(".active");
    const width = (((activeUI.length - 1) / (circleUI.length - 1)) * 100);
    progrssUI.style.width = `${width}%`;
    if( currentActive === 1) {
        previousBtnUI.disabled = true;
    } else if(currentActive === circleUI.length) {
        nextBtnUI.disabled = true;
    } else {
        previousBtnUI.disabled = false;
        nextBtnUI.disabled = false;
    }
}